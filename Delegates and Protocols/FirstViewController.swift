//
//  ViewController.swift
//  Delegates and Protocols
//
//  Created by Florentin on 19/07/2018.
//  Copyright © 2018 Florentin. All rights reserved.
//

import UIKit

// Step 2: Conform with protocol
class FirstViewController: UIViewController, CanReceive {
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // Send data forward - Step 1
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "sendDataForwards", sender: self)
        textField.text = ""
    }
    
    // send data forward - Step 2
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sendDataForwards" {
            
            let secondVC = segue.destination as! SecondViewController
            
            secondVC.data = textField.text!
            
            // Step 6: Set the second VC delegate as the current VC
            secondVC.delegate = self
        }
    }
    @IBAction func changeBackground(_ sender: Any) {
    
    view.backgroundColor = UIColor.blue
    }
    
    // Step 3: Implement required function from protocol
    func dataReceived(data: String) {
        label.text = data
    }
}
