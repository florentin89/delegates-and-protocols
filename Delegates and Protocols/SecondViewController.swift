//
//  SecondViewController.swift
//  Delegates and Protocols
//
//  Created by Florentin on 19/07/2018.
//  Copyright © 2018 Florentin. All rights reserved.
//

import UIKit

// Send data backward
// Step 1: Create protocol which has a name and a required method
protocol CanReceive {
    
    func dataReceived(data: String)
}

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    // Step 4: create a delegate OPTIONAL of protocol type
    var delegate: CanReceive?
    var data = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        label.text = data
    }

    // send data backward
    @IBAction func sendDataBack(_ sender: Any) {
    
        // Step 5: We trigger the method "dataReceived" and we send the value from the textfield backward to the first VC
        delegate?.dataReceived(data: textField.text!)
        
        // Step 7: Dismiss the SecondVC and go back to FirstVC
        dismiss(animated: true, completion: nil)
        
        textField.text = ""
    }
}
